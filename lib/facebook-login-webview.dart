import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class FacebookLoginWebView extends StatefulWidget {
  final String selectedUrl;
  FacebookLoginWebView({this.selectedUrl});

  @override
  _FacebookLoginWebViewState createState() => _FacebookLoginWebViewState();
}

class _FacebookLoginWebViewState extends State<FacebookLoginWebView> {
  final flutterWebview = new FlutterWebviewPlugin();

  @override
  void initState() {
    super.initState();

    flutterWebview.onUrlChanged.listen((url) {
      if (url.contains("#access_token")) {
        successCallback(url);
      } else if (url.contains(
          "https://www.facebook.com/connect/login_success.html?error=access_denied&error_code=200&error_description=Permissions+error&error_reason=user_denied")) {
        denyCallback();
      }
    });
  }

  void successCallback(String url) {
    final params = url.split("access_token=");
    final endParam = params[1].split("&");
    Navigator.pop(context, endParam[0]);
  }
  
  void denyCallback() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('Facebook Login'),
        backgroundColor: Color.fromRGBO(66, 103, 178, 1),
      ),
      url: widget.selectedUrl,
    );
  }
}

