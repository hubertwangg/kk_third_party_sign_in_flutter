import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class LoginProvider {
  static const GOOGLE = 'GO';
  static const FACEBOOK = 'FB';
  static const APPLE = 'AP';
}

class APIManager {
  static const BASE_URL = 'localhost:3001';
  static final _httpClient = http.Client();

  static Future<Map<String, dynamic>> thirdPartyLogin(String provider, {@required String firebaseUID, @required String fullName}) async {
    try {
      final url = Uri.http('$BASE_URL', '/customers/loginAuth');
      print('URL $url');
      final params = {
        'firebaseUID': firebaseUID,
        'fullName': fullName,
        'provider': provider,
      };

      final response = await _httpClient.post(url, body: params);
      return json.decode(response.body);
    } catch (err) {
      print('3rd party login error: $err');
      return {};
    }
  }
}