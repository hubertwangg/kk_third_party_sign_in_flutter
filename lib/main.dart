import 'package:flutter/material.dart';
import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:apple_sign_in/apple_sign_in_button.dart';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:signin_apple_test/facebook-login-webview.dart';
import 'package:signin_apple_test/api-manager.dart';

void main() {
  runApp(MaterialApp(
    title: 'Sign In(s) Demo',
    home: SignInDemo(),
  ));
}

class SignInDemo extends StatefulWidget {
  @override
  _SignInWithAppleState createState() => _SignInWithAppleState();
}

class _SignInWithAppleState extends State<SignInDemo> {
  bool isAppleSignInSupported = false;
  final String fbClientId = '275722563565406';
  final String fbLoginRedirectURL = 'https://sign-in-test-2a434.firebaseapp.com/__/auth/handler';

  @override
  void initState() {
    super.initState();

    if (Platform.isIOS) {
      AppleSignIn.onCredentialRevoked.listen((event) {
        print('Credential has been revoked');
      });

      DeviceInfoPlugin().iosInfo.then((iosInfo) {
        final version = iosInfo.systemVersion;
        final majorNumber = int.parse(version.split('.')[0]);

        this.isAppleSignInSupported = majorNumber >= 13;
      }).catchError((err) {
        print('Failed to obtain iOS version $err');
      });
    }
  }

  Widget notAvailableText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Text(
        'Sign in with Apple is only available on iOS 13.0+',
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 20,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget appleSignInButton() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: AppleSignInButton(
        style: ButtonStyle.black,
        type: ButtonType.defaultButton,
        onPressed: this.handleSignInWithApple,
      ),
    );
  }

  Widget facebookSignInButton() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: SignInButton(
        Buttons.Facebook,
        onPressed: this.handleSignInWithFacebook,
      ),
    );
  }

  Widget googleSignInButton() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: SignInButton(
        Buttons.Google,
        onPressed: this.handleSignInWithGoogle,
      ),
    );
  }

  void updateFirebaseProfile({@required String provider, String displayName}) async {
    try {
      // Test current user
      final currentUser = await FirebaseAuth.instance.currentUser();

      // Add to self database
      this.addToDatabase(provider, currentUser.uid, currentUser.displayName);

      // Update user data
      UserUpdateInfo updateUser = new UserUpdateInfo();
      updateUser.displayName = displayName;
      await currentUser.updateProfile(updateUser);

      print('Firebase Apple sign in config done! UID: ${currentUser.uid}, Name: ${currentUser.displayName}');
    } catch (err) {
      print('Firebase update profile failed: $err');
    }
  }

  void addToDatabase(String provider, String uid, String name) async {
    try {
      final userData = await APIManager.thirdPartyLogin(provider, firebaseUID: uid, fullName: name);
      print('Success added to database $userData');
    } catch (err) {
      print('Error on posting to DB: $err');
    }
  }

  void handleAppleFirebase(AppleIdCredential appleIDCredential) async {
    try {
      // Firebase sign-in code block
      final OAuthProvider authProvider = new OAuthProvider(providerId: 'apple.com');
      final AuthCredential credential = authProvider.getCredential(
          idToken: String.fromCharCodes(appleIDCredential.identityToken),
          accessToken: String.fromCharCodes(appleIDCredential.authorizationCode)
      );
      final AuthResult firebaseResult = await FirebaseAuth.instance.signInWithCredential(credential);
      final credentialProfile = firebaseResult.additionalUserInfo.profile;

      this.updateFirebaseProfile(provider: LoginProvider.APPLE, displayName: '${appleIDCredential.fullName.givenName} ${appleIDCredential.fullName.familyName}');
      print('Signed in by Apple with user info $credentialProfile');
    } catch (err) {
      print('Firebase Apple handling error: $err');
    }
  }

  void handleFacebookFirebase(AuthCredential facebookCredential) async {
    try {
      // Sign in with credential
      final AuthResult firebaseResult = await FirebaseAuth.instance.signInWithCredential(facebookCredential);
      final credentialProfile = firebaseResult.additionalUserInfo.profile;

      this.updateFirebaseProfile(provider: LoginProvider.FACEBOOK, displayName: credentialProfile['name']);
      print('Signed in by Facebook with user info $credentialProfile');
    } catch (err) {
      print('Firebase Facebook handling error: $err');
    }
  }

  void handleGoogleFirebase(AuthCredential googleCredential) async {
    try {
      // Sign in with credential
      final AuthResult firebaseResult = await FirebaseAuth.instance.signInWithCredential(googleCredential);
      final credentialProfile = firebaseResult.additionalUserInfo.profile;

      this.updateFirebaseProfile(provider: LoginProvider.GOOGLE, displayName: credentialProfile['name']);
      print('Signed in by Google with user info $credentialProfile');
    } catch (err) {
      print('Firebase Google handling error: $err');
    }
  }

  void handleSignInWithApple() async {
    // Sign in with Apple is available, pre-checked
    try {
      final AuthorizationResult authResult = await AppleSignIn.performRequests([
        AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName]),
      ]);

      switch (authResult.status) {
        case AuthorizationStatus.authorized:
          print('Sign in with Apple val ${authResult.credential}');
          final appleIDCredential = authResult.credential;
          this.handleAppleFirebase(appleIDCredential);
          break;
        case AuthorizationStatus.cancelled:
          print('Sign in with Apple cancelled');
          break;
        case AuthorizationStatus.error:
          print('Sign in with Apple error ${authResult.error}');
          break;
      }
    } catch (err) {
      print('Sign in with Apple error: $err');
    }
  }

  void handleSignInWithFacebook() async {
    try {
      String result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => FacebookLoginWebView(
            selectedUrl: 'https://www.facebook.com/dialog/oauth?client_id=$fbClientId&redirect_uri=$fbLoginRedirectURL&response_type=token&scope=email,public_profile,',
          ),
          maintainState: true,
        )
      );

      if (result != null) {
        final facebookCredential = FacebookAuthProvider.getCredential(accessToken: result);
        this.handleFacebookFirebase(facebookCredential);
      } else {
        print('Sign in with Facebook cancelled by user');
      }
    } catch (err) {
      print('Sign in with Facebook error: $err');
    }
  }

  void handleSignInWithGoogle() async {
    try {
      final GoogleSignIn googleSignIn = new GoogleSignIn(scopes: [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile'
      ]);
      final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

      // Get credential
      final AuthCredential authCredential = GoogleAuthProvider.getCredential(idToken: googleSignInAuthentication.idToken, accessToken: googleSignInAuthentication.accessToken);
      this.handleGoogleFirebase(authCredential);
    } catch (err) {
      print('Google sign in failed $err');
    }
  }

  Widget buildAndroidBody() {
    return this.notAvailableText();
  }

  Widget buildIOSBody() {
    List<Widget> children = [];

    children.add(this.facebookSignInButton());
    children.add(this.googleSignInButton());
    if (this.isAppleSignInSupported) {
      children.add(this.appleSignInButton());
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: children,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Sign in with Apple'),
        ),
        body: Container(
          child: Center(
            child: Platform.isIOS ? this.buildIOSBody() : this.buildAndroidBody(),
          ),
        ),
      ),
    );
  }
}
